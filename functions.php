<?php add_action('wp_enqueue_scripts', 'theme_enqueue_styles');
function theme_enqueue_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('child-style', get_stylesheet_uri(), array('parent-style'));
}

function load_comit_scripts()
{

    wp_register_script('click_track', ("/wp-content/custom/js/click-track.js"), array('jquery'), false, false);
    wp_enqueue_script('click_track');
}

add_action('wp_enqueue_scripts', 'load_comit_scripts');