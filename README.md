# leonieye.com #
![Build Status](https://bamboo.comit-hosting.com/plugins/servlet/wittified/build-status/COMIT-LEONIEYE)[![Built with Grunt](https://cdn.gruntjs.com/builtwith.png)](http://gruntjs.com/)

### What is this repository for? ###

* Wordpress Theme

Gravity Forms:
f7385791b5732441615955b25b3576d8

Elegant Themes:
comitdevelopers
fdc4e048caee0c5de2a7f6d41a543aa1f041e72e

### How do I get set up? ###

* grunt-source
* navigate to http://localhost:8080
* enter site name
* set theme to active
* import the wordpress export

### Contribution guidelines ###

* Write tests using Grunt and add them to the lint task
* Code must past lint after commited to develop in order for it to merge into master

### Who do I talk to? ###

* administrator@comitdevelopers.com
* development@comitdevelopers.com